function accuracy = VALIDATION( valid_set, node )
% VALIDATION	Calculates accuracy of the label learnt from decision tree 
%               and actual label from dataset
% args:
%	valid_set	- validation set
%	node		- tree/subtree
% return
%	accuracy	- returns accuracy of of learnt labels from actual labels

%% Extracting rows and cols from 'valid_set'
[ rows, cols ] = size( valid_set );
label = valid_set( :, cols );
data = valid_set( :, 1:cols-1 );

%% pre-allocating size to tlabel for quicker-execution
tlabel = zeros( 1, rows );

%% Computing labels from decision tree
for i = 1:rows
    % if node.value is true or false, it means we have reached to leaf.
    % Therefore it needs to be stored in 'tlabel' accordingly.
    if( strcmp( node.value, 'true') || ~strcmp( node.value, 'false' ) )
        element = data( i, node.value );
        if( element == 1 )
            tlabel(i) = 1;
        elseif( element == 0 )
            tlabel(i) = 0;
        end
    
    % if node.value isn't true or false, but index value, it means we are
    % on some level of the tree. we need to traverse deeper to find label
    % := hence recursion below
    else
        element = data( i, node.value );
        if( element == 1 )
            VALIDATION( data, node.right );
        elseif( element == 0 )
            VALIDATION( data, node.left );
        end
        
    end
end

%% Calculating accuracy by comparing label and tlabel (which we received from tree)
accuracy = sum( label' == tlabel )*100 / rows;

end