function [f_rows, f_cols] = FILE_LENGTH( fname )
%FILE_LENGTH	Calculates number of rows and columns in the file read 
%               using csvread. It can be further used to extract special 
%               features like annotation, descriptors etc.
%args:
%	fname		- filename of the file to calculate rows and columns
%
%return:
%	f_rows		- returns number of rows in the file
%	f_cols		- returns number of columns in the file

%% variables
colstat = 0;
f_cols = 0;
f_rows = 0;

%% Formatting the options to make long based operations while reading from file.
format long;

%% opening file
fid = fopen( fname, 'r' );
if fid < 0
    disp( 'fid < 0. File inaccessible.' );
else
    %% total number of rows and columns in dataset
    while 1
        line = fgetl( fid );
        if line < 0
            fclose( fid );
            break;
        end
        
        if colstat == 0
            f_cols = length( find( line == ',' ) ) + 1;
            colstat = 1;
        end
        
        f_rows = f_rows + 1;
    end
end