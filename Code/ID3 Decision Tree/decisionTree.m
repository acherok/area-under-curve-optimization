function accuracy = decisionTree( fname )
% DECISION_TREE	main function to execute the program. 
% args:
%	fname: 		- filename of the dataset to be tested
% return:
%	accuracy:	- returns the accuracy between validation dataset and 
%                 value returned from decision tree

%% Extracting dataset from fname
dataset = csvread( fname );
[ rows, cols ] = FILE_LENGTH( fname );

%% Dividing dataset into training and validation dataset
[ train_set, valid_set ] = DIVIDE8020( dataset, rows, cols );

%% passing target data and label to ID3 algorithm
root = ID3_ALGO( train_set );

%% Printing tree
% PRINTTREE( root, root.value );

%% validating with valid set
accuracy = VALIDATION( valid_set, root );

end