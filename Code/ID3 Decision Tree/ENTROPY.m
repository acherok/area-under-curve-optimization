function ent = ENTROPY( pos, neg )
% ENTROPY	calculates entropy of passed values
% args:
%	pos, neg	- input as single element and array 
% return:
%	ent		- returns entropy of 'pos' and 'neg'

%% pos and neg to calculate entropy
if( pos ~= 0 | neg ~= 0 )
    sum = pos + neg;
    
    pos( pos == 0 ) = NaN;
    m1 = pos .* log2( pos );
    m1( isnan( m1 ) ) = 0;
    
    neg( neg == 0 ) = NaN;
    m2 = neg .* log2( neg );
    m2( isnan( m2 ) ) = 0;
    
    m3 = 1./sum;
    ent = -1 * m3 .* ( m1 + m2 );
else
    ent = 0;
end