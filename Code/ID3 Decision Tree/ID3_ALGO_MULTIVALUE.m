function [ tree ] = ID3_ALGO( dataset )
% ID3_ALGO   Runs the binary decision tree algorithm on training dataset
% args:
%	dataset		- matrix of 1s and 0s for each attribute. Every attribute
%                 has either 0 or 1, showing true or false value. Last
%                 column is the label attribute.
% return:
%	node		- Root node of decision tree
% tree structure
%	value		- It stores the index of the attribute which has maximum
%                 information gain.
%	branch      - It has the subset of the parent dataset based on IG.

%% dataset related variables
length = size( dataset );
rows = length(1);
cols = length(2);
features = cols - 1;
label = dataset( :, cols );

%% Exit condition of recursion
if( sum( dataset( :, cols ) ) == rows )
    tree.value = 'true';
    return
end

if( sum( dataset( :, cols ) ) == 0 )
    tree.value = 'false';
    return;
end

if( cols == 1 )
    if( sum( label ) >= rows/2 )
        tree.value = 'true';
    else
        tree.value = 'false';
    end
    return;
end

%% creating node
% typedef struct _node_t
% {
%   void *data;
%   struct _node_t **branch;
% } node_t;
tree = struct( 'value', 'null', 'node', struct( 'edge', [], 'branch', [] ) );

%% ( S+, S- ) from provided dataset
S_pos = sum( label == 1 );
S_neg = sum( label == 0 );

%% Finding unique values from each feature
for i = 1:features
    ufx{ i } = unique( dataset( :, i ) );   % unique values in dataset from each feature
    uf( i ) = numel( ufx{ i } );    % number of elements in each cell
end

%% ( A_0+, A_0- ) and ( A_1+, A_1- ). A_0+ and A_0- are vectors of length = number of features and A_1+ and A_1- are vectors of length = number of features

for i = 1:features
    for j = 1:uf(i)
        pos( j ) = sum( dataset( dataset(:,i) == ufx{i}(j) & label == 1, i ) == ufx{i}(j) );
        neg( j ) = sum( dataset( dataset(:,i) == ufx{i}(j) & label == 0, i ) == ufx{i}(j) );
    end
    A_pos{ i } = pos;
    A_neg{ i } = neg;
end

%% Information Gain ( IG )
EntS = ENTROPY( S_pos, S_neg );
AbsS = ABSOLUTE( S_pos, S_neg );
ig = zeros( 1, features );

for i = 1:features
    EntA{ i } = ENTROPY( A_pos{ i }, A_neg{ i } );
    AbsA{ i } = ABSOLUTE( A_pos{ i }, A_neg{ i } );
    
    ig( i ) = EntS - ( ( 1 / AbsS ) * sum( AbsA{ i } .* EntA{ i } ) );
end

%% Maximum IG.
[ ~, index ] = max( ig );

%% Extracting 'index' column. AmaxIG: attribute having maximum IG.
AmaxIG = dataset( :, index );

%% Creating unique value specific branches
for i = 1:uf( index )
    index
    set = dataset( AmaxIG == ufx{ index }( i ), : )
    set( :, index ) = [];
    tree.value = index;
    tree.node.edge = ufx{index}(i);
    tree.node.branch = ID3_ALGO_MULTIVALUE( set );
    sprintf(' %d  %d ', tree.value, tree.node.edge )
    
end

end