%{
In your ID3_ALGO.m

1. You can add at least the following to your exit condition of recursion:

   1.1 If the number of split reaches certain number. This number can be an adjustable value, which in turn can be tweaked by GA or PSO.
   1.2 If the depth of the tree reaches certain number. This number can be an adjustable value.

2. You used the "rows/2" as the criteria to classify the leaf. Actually,

   2.1 You can make the "2" an adjustable value.
   2.2 Or you can just return the percentage of each class in the leaf, and turn the classification tree into regression tree which will give "confidence" value of a node belonging to certain class.

3. Regarding how to expand to multiple-value feature, 

   3.1 You will have to make the A_0_x, A_1_x a two dimensional data structure. Like A_pos being a array of struct, and each of struct contains its own array (because each feature will have diffrent numbers of unique values).
   3.2 In the "for i = 1:cols-1" loop, you will have to add another for loop through all posible values for individual feature.

4. Regarding the binary tree vs multi-branch tree

   4.1 At each node, you just consider feature == value and feature <> feature cases (no matter how many different unique values a feature has). This way, you keep building a binary tree.
   4.2 Or you will have to change your recursive-function-call manner of programming to a big loop manner to build a multi-branch tree.

%}