function a = ABSOLUTE( x, y )
% ABSOLUTE	calculates absolute value of sum of passed parameters
% args:
%	x,y		- input as single element and array 
% return:
%	a		- returns absolute value of sum of 'x' and 'y'

a = abs( x + y );
end