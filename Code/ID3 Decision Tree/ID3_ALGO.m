function [ tree ] = ID3_ALGO( dataset )
% ID3_ALGO   Runs the binary decision tree algorithm on training dataset
% args:
%	dataset		- matrix of 1s and 0s for each attribute. Every attribute 
%                 has either 0 or 1, showing true or false value. Last 
%                 column is the label attribute.
% return:
%	tree		- Root node of decision tree
% tree structure
%	value		- It stores the index of the attribute which has maximum 
%                 information gain.
%	left		- pointer to another node. left means that the attribute 
%                 was false
%	right		- pointer to another node. right means that the attribute 
%                 was true.

%% dataset related variables
length = size( dataset );
rows = length(1);
cols = length(2);
label = dataset( :, cols );

%% Exit condition of recursion
if( sum( dataset( :, cols ) ) == rows )
    tree.value = 'true';
    return
end

if( sum( dataset( :, cols ) ) == 0 )
    tree.value = 'false';
    return;
end

if( cols == 1 )
    if( sum( label ) >= rows/2 )
        tree.value = 'true';
    else
        tree.value = 'false';
    end
    return;
end

%% creating tree node
tree = struct( 'value', 'null', 'left', 'null', 'right', 'null' );

%% ( S+, S- ) from provided dataset
S_pos = sum( label == 1 );
S_neg = sum( label == 0 );

%% ( A_0+, A_0- ) and ( A_1+, A_1- ). A_0+ and A_0- are vectors of length = number of features and A_1+ and A_1- are vectors of length = number of features
% can use 'unique' command to include multiple values like weather = { sunny, windy, rainy }.
A_0_pos = zeros( 1, cols-1 );
A_0_neg = zeros( 1, cols-1 );
A_1_pos = zeros( 1, cols-1 );
A_1_neg = zeros( 1, cols-1 );

for i = 1:cols-1
    
    A_0_pos( i ) = sum( dataset( ( dataset(:, i) == 0 & label == 1 ), i ) == 0 );
    A_0_neg( i ) = sum( dataset( ( dataset(:, i) == 0 & label == 0 ), i ) == 0 );
    
    A_1_pos( i ) = sum( dataset( ( dataset(:, i) == 1 & label == 1 ), i ) == 1 );
    A_1_neg( i ) = sum( dataset( ( dataset(:, i) == 1 & label == 0 ), i ) == 1 );

end

%% Information Gain ( IG )
EntS = ENTROPY( S_pos, S_neg );
AbsS = ABSOLUTE( S_pos, S_neg );

EntA0 = ENTROPY( A_0_pos, A_0_neg );
AbsA0 = ABSOLUTE( A_0_pos, A_0_neg );

EntA1 = ENTROPY( A_1_pos, A_1_neg );
AbsA1 = ABSOLUTE( A_1_pos, A_1_neg );

information_gain = ENTROPY( S_pos, S_neg ) - (( 1 / AbsS ) * ( AbsA0 .* EntA0 ) + ( AbsA1 .* EntA1 ));

%% Maximum IG.
[ ~, index ] = max( information_gain );

%% Extracting 'index' column
attribute_maxIG = dataset( :, index );

%% Creating left tree where the value of the attribute is zero.
left_subset = dataset( attribute_maxIG == 0, : );
left_subset( :, index ) = [];

%% Creating right tree where the value of the attribute is one.
right_subset = dataset( attribute_maxIG == 1, : );
right_subset( :, index ) = [];

%% Setting that node as maximum IG.
tree.value = index;

%% Assigning left node splits
tree.left = ID3_ALGO( left_subset );

%% Assigning right node splits (=left or right)
tree.right = ID3_ALGO( right_subset );

end