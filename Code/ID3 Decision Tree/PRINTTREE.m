function [] = PRINTTREE( tree, parent )
% Pre-order traversal

%% Printing current node and exit condition for recursion

if( strcmp( tree.value, 'true' ) )
    fprintf( 'parent: %s\ttrue\n', parent );
    return;
elseif( strcmp( tree.value, 'false' ) )
    fprintf( 'parent: %s\tfalse\n', parent );
    return;
else
    fprintf('parent: %s\tattribute: %s\tfalseChild:%s\ttrueChild:%s\n', parent, tree.value, tree.left.value, tree.right.value);
end

%% Traversing left and right tree
PRINTTREE( tree.left, tree.value );
PRINTTREE( tree.right, tree.value );

end