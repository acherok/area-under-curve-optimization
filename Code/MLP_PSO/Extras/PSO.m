function[meany]=PSO_Final_alt(data,label)
%  The implementation of Particle Swarm Optimization. 
%  ========================================================================
%  Input:
%  PSO_Final_alt takes  3 input parameters
%
%    data =>a matrix with size t_rows * t_cols. Each row vector 
%               is a instance vector (including their labels)
%
%    label =>a vectore with size instanceNum, which correspends
%                 the instance's label
%
%    inertia=>the particle inertia 
%
%    correction_factor => co-efficent for intensification/diversication
%
%  
%  ========================================================================
%  Output:
%    w          the learned linear classifier weights
%  ========================================================================

%%Intialize the method parameters
maxIter =1000;                               % maximum number of iterations
inertia = 0.00185;                           % intertia of particle
correction_factor =0.0035;                   % correction factor for weights
[t_rows,t_cols]=size(data);
t_cols=t_cols+1;
%% Initialise the particle values
weight(1,1:t_cols-1) = 1.0*rand(1,t_cols-1);
velocity(1,1:t_cols-1)=1.0.*rand(1,t_cols-1);
local_Error(1,1:t_cols)=0;
local_Error(1,t_cols)=1000;
global_Error(1,1:t_cols)=0;
global_Error(1,t_cols)=1000;

%% The objective function is objf_alt
objf_alt= @(X,W)(X*W');

%% The main loop of PSO
for iter = 1:maxIter
    % update weights by velocity
    weight(1,:)=weight(1,:)+ velocity(1,:)/3.17;     
    fval=objf_alt(data,weight);
    % Update local best value
    for ii=1:length(fval)
        if fval(ii,1) <local_Error(1,t_cols)         
            local_Error(1,1:t_cols-1)=weight(1,:);
            local_Error(1,t_cols)=fval(ii,1);
        end
    end
    % Update global best value
    if local_Error(1,t_cols)<global_Error(1,t_cols)  
        global_Error(1,1:t_cols-1)=local_Error(1,1:t_cols-1);
        global_Error(1,t_cols)=local_Error(1,t_cols);
    end
    % Update the velocities of the particles
    velocity(1,:)= inertia.*(1.0.*rand(1,t_cols-1).*velocity(1,:))+...
        correction_factor.*(1.0.*rand(1,t_cols-1).*(local_Error(1,t_cols-1)-velocity(1,:)))+...
        correction_factor.*(1.0.*rand(1,t_cols-1).*(global_Error(1,t_cols-1)-velocity(1,:)));
end
%% optimized 
%weight(1,:)=global_Error(1,1:t_cols-1);
meany=global_Error(1,t_cols);

