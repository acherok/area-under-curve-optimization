%% Clearing display and previous stored variables
clear all
clc

%% To enable long type read from file
format long

%% Set initial state of random numbers
rng( 'default' );
rng( 0 );

%% file contents
fname = 'E:\glass0.dat';
dataset = csvread( fname );
[ t_rows, t_cols ] = size( dataset );
t_rows_80 = floor( 0.8*t_rows );
t_rows_20 = t_rows - t_rows_80;

%% separating training and validation data
[ train_data, train_label, valid_data, valid_label  ] = divide8020( dataset, t_rows, t_cols );

training_data = train_data;
training_label = train_label;
validation_data = valid_data;
validation_label = valid_label;

eta= 0.00015;
% MLP(dataset,eta)
% PSO_eta(dataset)
PSO_weight(dataset,eta)
tic;
%train_weights=(train_data,train_label);
%eta=0.0002;
%for i=1:1000
%acc(i,1)=MLP(dataset,eta);
%if acc(i,1)==max(acc)
%    disp(eta);
%    disp(acc(i,1));
%end
%eta=eta-0.000005;
%end