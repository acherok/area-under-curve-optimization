%% Clearing display and previous stored variables
clear all
clc

%% To enable long type read from file
format long

%% Set initial state of random numbers
rng( 'default' );
rng( 0 );

%% file contents
fname = 'E:\glass0.dat';
dataset = csvread( fname );

eta= 0.004976191854319; % place optimized eta value;
PSO_weight(dataset,eta);

%% Display properties
figure( 1 );
plot( area );
title( 'Area graph of 100 iterations' );

[ value, index ] = max( area );
p = squeeze( prec( index, :, : ) );
r = squeeze( recc( index, :, : ) );

figure( 2 );
plot( r, p, 'o' )
title( 'PR Curve' );

area = trapz( r, p )
wx = squeeze( nw( index, :, : ) );




