function[ PRECISION, RECALL, TPR, FPR ]=MLP(dataset,eta)
[ t_rows, t_cols ] = size( dataset );
t_rows_80 = floor( 0.8*t_rows );
t_rows_20 = t_rows - t_rows_80;

%% separating training and validation data
[ train_data, train_label, valid_data, valid_label  ] = divide8020( dataset, t_rows, t_cols );

training_data = train_data;
training_label = train_label;
validation_data = valid_data;
validation_label = valid_label;


%% control variables
iterations = 100;
%eta = 0.00015;
alpha = 0.02;

%% Layer size information
R = floor( 0.8*t_rows ); % number of instances
I = t_cols - 1; % number of input features
J = 3; % number of units in hidden layer
K = 1; % number of units in output layer

%% Weights initialization
v_ij = rand( I+1, J ); % 1 added because of bias weight; between input and hidden layer
%vij=PSO_Final(train_data,train_label,t_cols);
w_jk = rand( J+1, K ); % between hidden and output layer

%% Standarization: Converting the datasets to mean 0 and standard deviation 1
data_avg = mean( training_data );
data_shift = training_data - repmat( data_avg, R, 1 );
data_std = std( data_shift );
data = data_shift ./ repmat( data_std, R, 1 );

label = training_label; % no need to standarize this as its either 0 or 1

Error = zeros( iterations, 1 ); %zero filled array for future use.

%% Feedforward traversal
for i=1:iterations
    
    r = randi( t_rows_80 );
    
    x_r_i = [ data( r, : ), 1]';
    a_r_j = v_ij' * x_r_i; 
    z_r_j_raw = a_r_j; % identity function g(x) = x
    
    z_r_j = [ z_r_j_raw; 1 ];
    a_r_k = w_jk' * z_r_j; 
    y_r = a_r_k; % identity function g(x) = x
    
    diff_r_n = label( r, : )' - y_r;
    Error( i ) = 1/2 * sqrt( sum( diff_r_n.^2 ) );
    
    %fprintf( 'The %dth iteration, the %dth sample, squared error %f\n', i,r, Error( i ) );
    
    delta_k = diff_r_n;
    diff_w_jk = eta * z_r_j * delta_k';
    
    delta_j = w_jk( 1:J, :) * delta_k;
    diff_v_ij = eta * x_r_i * delta_j';
    
    if i==1
        v_ij = v_ij + diff_v_ij;
        w_jk = w_jk + diff_w_jk;
    else
        v_ij = v_ij + (1-alpha)*diff_v_ij + alpha*prev_diff_v_ij;
        w_jk = w_jk + (1-alpha)*diff_w_jk + alpha*prev_diff_w_jk;
    end
    
    prev_diff_v_ij = diff_v_ij;
    prev_diff_w_jk = diff_w_jk;
end

%% Validation of weights on the data
x = [ validation_data, ones( t_rows_20, 1 ) ];

aj = x * v_ij;
z_raw = aj;

z = [ z_raw, ones( t_rows_20, 1 ) ];
ak = z * w_jk;
y = ak;

yx = y;
meany = mean( y );
%meany = PSO(valid_data,valid_label);
y( y>=meany ) = 1;
y( y<meany & y~=1 ) = 0;

accuracy = sum( y==validation_label )/t_rows_20*100;

%% Confusion matrix. Recall and Precision. ROC
confusion_mat = [ validation_label, y ];

TP = sum( confusion_mat(:,1)==1 & confusion_mat(:,2)==1 );
TN = sum( confusion_mat(:,1)==0 & confusion_mat(:,2)==0 );
FN = sum( confusion_mat(:,1)==1 & confusion_mat(:,2)==0 );
FP = sum( confusion_mat(:,1)==0 & confusion_mat(:,2)==1 );

RECALL = TP/(TP+FN);
PRECISION = TP/ (TP+FP);

TPR = TP/(TP+FN);
FPR = FP/(TN+FP);
