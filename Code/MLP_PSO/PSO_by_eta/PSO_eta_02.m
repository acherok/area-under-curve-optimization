function[] = PSO_eta( dataset )
%  The implementation of Particle Swarm Optimization.
%  ========================================================================
%  Input:
%  PSO_Final_alt takes  3 input parameters
%
%    data =>a matrix with size t_rows * t_cols. Each row vector
%               is a instance vector (including their labels)
%
%    label =>a vectore with size instanceNum, which correspends
%                 the instance's label
%
%    inertia=>the particle inertia
%
%    correction_factor => co-efficent for intensification/diversication
%
%
%  ========================================================================
%  Output:
%    w          the learned linear classifier weights
%  ========================================================================

format long
rng( 'default' );
rng( 0 );

%%Intialize the method parameters
maxIter = 100;                               % maximum number of iterations
inertia = 0.00185;                           % intertia of particle
correction_factor = 0.0035;                   % correction factor for weights

%% Initialise the particle values
%eta = 8.081176679289340e-04;
eta = 0.0001;
new_eta = eta;

velocity = 0.001 * rand;
local_best = 0;
local_best_eta = 0;
global_best = 0;
global_best_eta = 0;

e = zeros(1,maxIter);
rec = zeros(1,maxIter);
prec = zeros(1,maxIter);
tpr = zeros(1,maxIter);
fpr = zeros(1,maxIter);

%% BAAP Loop
for i = 1:100
    
    eta = new_eta;
    %% The main loop of PSO
    for iter = 1:maxIter
        % update weights by velocity
        eta = eta + velocity/3.17;
        ETA( iter ) = eta;
        
        [ fval, rec, TPR, FPR ] = MLP( dataset, eta );
        
        PRECISION( iter ) = fval;
%         prec(i,iter) = fval;
        RECALL( iter ) = rec;
%         recc(i,iter) = rec;
        
        % Update local best value
        for ii = 1:length( fval )
            if fval(ii,1) > local_best
                local_best_eta = eta;
                local_best = fval;
            end
        end
        % Update global best value
        if local_best > global_best
            global_best_eta = local_best_eta;
            global_best = local_best;
        end
        % Update the velocities of the particles
        velocity(1,:) = inertia *( 1.00000 * rand * velocity )+...
            correction_factor * ( 1.00000 * rand *( local_best_eta - velocity ) )+...
            correction_factor *( 1.00000 * rand * ( global_best_eta - velocity ) );
    end
    
    [ value, index ] = max( PRECISION );
    prec(i,:,:) = PRECISION;
    recc(i,:,:) = RECALL;
    new_eta = ETA(index);
    neta( i ) = new_eta;
    pr_auc(i) = trapz( RECALL, PRECISION );
    
    assignin( 'base', 'area', pr_auc );
    assignin( 'base', 'neta', neta );
    assignin( 'base', 'prec', prec );
    assignin( 'base', 'recc', recc );
    
end

% e(iter) = eta;
% assignin( 'base', 'ETA', e );
% 
% rec(iter) = global_best;
% assignin( 'base', 'RECALL', rec );
% 
% prec(iter) = PRECISION;
% prec( isnan(prec) ) = 0;
% assignin( 'base', 'PRECISION', prec );
% 
% tpr(iter) = TPR;
% tpr( isnan(tpr) ) = 0;
% assignin( 'base', 'TPR', tpr );
% 
% fpr(iter) = FPR;
% fpr( isnan(fpr) ) = 0;
% assignin( 'base', 'FPR', fpr );