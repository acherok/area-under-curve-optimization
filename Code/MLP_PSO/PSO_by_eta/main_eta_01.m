%% Clearing display and previous stored variables
clear all
clc

%% To enable long type read from file
format long

%% Set initial state of random numbers
rng( 'default' );
rng( 0 );

%% file contents
fname = 'E:\glass0.dat';
dataset = csvread( fname );

PSO_eta_02( dataset );

%% Display properties
figure( 1 );
plot( area );
title( 'Area graph of 100 iterations' );

[ value, index ] = max( area );
p = squeeze( prec( index, :, : ) );
r = squeeze( recc( index, :, : ) );

figure( 2 );
plot( r, p, 'o' )
title( 'PR Curve' );

area = trapz( r, p )
etaa = neta(1,81)

