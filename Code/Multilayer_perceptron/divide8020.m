function [ train_data, train_label, valid_data, valid_label ] = divide8020( dataset, t_rows, t_cols )

%% storing last column having label.
label = dataset( :, t_cols );

%% extracting class 0 and class 1 datasets
class0 = dataset( label==0, : );
class1 = dataset( label==1, : );

lengthC0 = length( class0 );
lengthC1 = length( class1 );

%% Calculating 80% and 20% of the length of dataset.
f0 = floor( 0.8*lengthC0 );
f0inv = lengthC0 - f0;

f1 = floor( 0.8*lengthC1 );
f1inv = lengthC1 - f1;

%% Partitioning values in appropriate arrays.
r = randperm( lengthC0 );
r1 = randperm( lengthC1 );

train_data( 1:f0, 1:t_cols-1 ) = class0( r( 1:f0 ), 1:t_cols-1 );
train_data( f0+1:f0+f1, : ) = class1( r1( 1:f1 ), 1:t_cols-1 );

train_label( 1:f0, : ) = class0( r( 1:f0 ), t_cols );
train_label( f0+1:f0+f1, : ) = class1( r1( 1:f1 ), t_cols);

valid_data( 1:f0inv, : ) = class0( r( f0+1:lengthC0 ), 1:t_cols-1 );
valid_data( f0inv+1:f0inv+f1inv, : ) = class1( r1( f1+1:lengthC1 ), 1:t_cols-1 );

valid_label( 1:f0inv, : ) = class0( r( f0+1:lengthC0 ), t_cols );
valid_label( f0inv+1:f0inv+f1inv ) = class1( r1( f1+1:lengthC1 ), t_cols );

end